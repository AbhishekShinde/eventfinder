import Foundation
import UIKit

final class EventDetailsUIViewController: UIViewController {

  @IBOutlet weak var eventImage: CustomImageView!
  @IBOutlet weak var venue: UILabel!
  @IBOutlet weak var dateTime: UILabel!

  private let event: Event

  init?(coder: NSCoder, event: Event) {
    self.event = event
    super.init(coder: coder)
  }
  
  required init?(coder: NSCoder) {
      fatalError("Use `init(coder:image:)` to initialize an `EventDetailsUIViewController` instance.")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupUI(title: event.title)
  }

  override public var preferredStatusBarStyle: UIStatusBarStyle {
    return .darkContent
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.navigationBar.tintColor = .black
    eventImage.image = nil
    if let dateString = event.datetimeUTC {
      dateTime.text = dateString.convertDateFormat()
    }
    venue.text = event.venue?.displayLocation
    eventImage.layer.cornerRadius = 10
    guard let imageString = event.performers?.first?.image else { return }
    eventImage.downloadImageFrom(urlString: imageString)
  }
  
  private func setupUI(title: String? = "") {
    let label = UILabel()
    label.backgroundColor = .clear
    label.numberOfLines = 2
    label.font = UIFont.boldSystemFont(ofSize: 18.0)
    label.textAlignment = .left
    label.textColor = .black
    label.text = title
    self.navigationItem.titleView = label
  }
}
